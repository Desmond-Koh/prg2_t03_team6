//============================================================
// Student Number : S10208193, S10206327
// Student Name : Desmond Koh, Herman Cho
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_Monitoring_System
{
    class TraceTogetherToken
    {
        public string SerialNo { get; set; }
        public string CollectionLocation { get; set; }
        public DateTime ExpiryDate { get; set; }

        public TraceTogetherToken() { }

        public TraceTogetherToken(string serialNo, string collectionLocation, DateTime expiryDate)
        {
            SerialNo = serialNo;
            CollectionLocation = collectionLocation;
            ExpiryDate = expiryDate;
        }

        public bool IsEligibleForReplacement()
        {
            if (ExpiryDate.AddMonths(1) >= DateTime.Now)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ReplaceToken()
        {
            Random num = new Random();
            SerialNo = "T" + num.Next(10000, 99999);
            ExpiryDate = DateTime.Now.AddMonths(6);
            Console.WriteLine("The Trace Together Token has been replaced");
        }

        public override string ToString()
        {
            return "\nSerial No: " + SerialNo +
                "\nCollection Location: " + CollectionLocation +
                "\nExpireDate: " + ExpiryDate;
        }
    }
}
