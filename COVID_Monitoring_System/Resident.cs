﻿//============================================================
// Student Number : S10208193, S10206327
// Student Name : Desmond Koh, Herman Cho
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_Monitoring_System
{
    class Resident : Person
    {
        public string Address { get; set; }
        public DateTime LastLeftCountry { get; set; }
        public TraceTogetherToken Token { get; set; }

        public Resident(string name, string address, DateTime lastLeftCountry) : base(name)
        {
            Address = address;
            LastLeftCountry = lastLeftCountry;
        }

        public override double CalculateSHNCharges()
        {
            for (int i = 0; i < TravelEntryList.Count; i++)
            {
                foreach (TravelEntry t in TravelEntryList)
                {
                    if (t.LastCountryOfEmbarkation == "New Zealand" || t.LastCountryOfEmbarkation == "Vietnam")
                    {
                        return 200;
                    }
                    else if (t.LastCountryOfEmbarkation == "Macao SAR")
                    {
                        return 220;
                    }
                    else
                    {
                        return 1220;
                    }
                }
            }
            return 0;
        }

        public override string ToString()
        {
            return base.ToString() +
                "\nAddress: " + Address +
                "\nLast Left Country: " + LastLeftCountry;
        }


    }
}
