﻿//============================================================
// Student Number : S10208193, S10206327
// Student Name : Desmond Koh, Herman Cho
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_Monitoring_System
{
    class SHNFacility
    {
        public string FacilityName { get; set; }
        public int FacilityCapacity { get; set; }
        public int FacilityVacancy { get; set; }
        public double DistFromAirCheckpoint { get; set; }
        public double DistFromSeaCheckpoint { get; set; }
        public double DistFromLandCheckpoint { get; set; }

        public SHNFacility() { }
        public SHNFacility(string facilityName, int facilityCapacity, double distFromAirCheckpoint, double distFromSeaCheckpoint, double distFromLandCheckpoint)
        {
            FacilityName = facilityName;
            FacilityCapacity = facilityCapacity;
            DistFromAirCheckpoint = distFromAirCheckpoint;
            DistFromSeaCheckpoint = distFromSeaCheckpoint;
            DistFromLandCheckpoint = distFromLandCheckpoint;
        }

        public double CalculateTravelCost(string entryMode, DateTime entryDate)
        {
            double base_fare = 0;

            if (entryMode == "Air")
            {
                base_fare = 50 + DistFromAirCheckpoint * 0.22;
            }
            else if (entryMode == "Sea")
            {
                base_fare = 50 + DistFromSeaCheckpoint * 0.22;
            }
            else if (entryMode == "Land")
            {
                base_fare = 50 + DistFromLandCheckpoint * 0.22;
            }

            TimeSpan time = Convert.ToDateTime(entryDate).TimeOfDay;
            if (((time >= new TimeSpan(6, 0, 0)) && (time <= new TimeSpan(8, 5, 9))) || ((time >= new TimeSpan(18, 0, 0)) && (time <= new TimeSpan(23, 5, 9))))
            {
                base_fare = base_fare * 1.25;
            }
            else if ((time >= new TimeSpan(0, 0, 0)) && (time <= new TimeSpan(5, 5, 9)))
            {
                base_fare = base_fare * 1.50;
            }

            return base_fare;
        }

        public bool IsAvailable()
        {
            if (FacilityVacancy != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return "Facility Name: " + FacilityName +
                "\nFacility Capacity: " + FacilityCapacity +
                "\nDistance from Air Checkpoint: " + DistFromAirCheckpoint +
                "\nDistance from Sea Checkpoint: " + DistFromSeaCheckpoint +
                "\nDistance from Land Checkpoint: " + DistFromLandCheckpoint;
        }
    }
}
