﻿//============================================================
// Student Number : S10208193, S10206327
// Student Name : Desmond Koh, Herman Cho
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_Monitoring_System
{
    class TravelEntry
    {
        public string LastCountryOfEmbarkation { get; set; }
        public string EntryMode { get; set; }
        public DateTime EntryDateTime { get; set; }
        public DateTime ShnEndDate { get; set; }
        public SHNFacility ShnStay { get; set; }
        public bool isPaid { get; set; }

        public TravelEntry() { }

        public TravelEntry(string lastCountryOfEmbarkation, string entryMode, DateTime entryDateTime)
        {
            LastCountryOfEmbarkation = lastCountryOfEmbarkation;
            EntryMode = entryMode;
            EntryDateTime = entryDateTime;
        }

        public void AssignSHNFacility(SHNFacility s)
        {
            ShnStay = s;
        }

        public void CalculateSHNDuration()
        {
            if (LastCountryOfEmbarkation == "New Zealand" || LastCountryOfEmbarkation == "Vietnam")
            {
                ShnEndDate = EntryDateTime;
            }
            else if (LastCountryOfEmbarkation == "Macao SAR")
            {
                ShnEndDate = EntryDateTime.AddDays(7);
            }
            else
            {
                ShnEndDate = EntryDateTime.AddDays(14);
            }
        }

        public override string ToString()
        {
            return "Last Country of Embarkation: " + LastCountryOfEmbarkation +
                "\nEntry Mode: " + EntryMode +
                "\nEntry Date Time: " + EntryDateTime;
        }
    }
}
