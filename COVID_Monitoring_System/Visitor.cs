//============================================================
// Student Number : S10208193, S10206327
// Student Name : Desmond Koh, Herman Cho
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_Monitoring_System
{
    class Visitor : Person
    {
        public string PassportNo { get; set; }
        public string Nationality { get; set; }

        public Visitor(string name, string passportNo, string nationality) : base(name)
        {
            PassportNo = passportNo;
            Nationality = nationality;
        }

        public override double CalculateSHNCharges()
        {
            for (int i = 0; i < TravelEntryList.Count; i++)
            {
                foreach (TravelEntry t in TravelEntryList)
                {
                    if (t.LastCountryOfEmbarkation == "New Zealand" || t.LastCountryOfEmbarkation == "Vietnam" || t.LastCountryOfEmbarkation == "Macao SAR")
                    {
                        return 280;
                    }
                    else
                    {
                        return 2200;
                    }
                }
            }
            return 0;
        }

        public override string ToString()
        {
            return base.ToString() +
                "\nPassport Number: " + PassportNo +
                "\nNationality: " + Nationality;
        }
    }
}
