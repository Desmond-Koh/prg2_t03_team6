﻿//============================================================
// Student Number : S10208193, S10206327
// Student Name : Desmond Koh, Herman Cho
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_Monitoring_System
{
    abstract class Person
    {
        public string Name { get; set; }
        public List<SafeEntry> SafeEntryList { get; set; } = new List<SafeEntry>();
        public List<TravelEntry> TravelEntryList { get; set; } = new List<TravelEntry>();

        public Person() { }

        public Person(string name)
        {
            Name = name;
            SafeEntryList = new List<SafeEntry>();
            TravelEntryList = new List<TravelEntry>();
        }

        public void AddTravelEntry(TravelEntry travelEntry)
        {
            TravelEntryList.Add(travelEntry);
        }

        public void AddSafeEntry(SafeEntry safeEntry)
        {
            SafeEntryList.Add(safeEntry);
        }

        public abstract double CalculateSHNCharges();

        public override string ToString()
        {
            return "Name: " + Name;
        }

    }
}
