﻿//============================================================
// Student Number : S10208193, S10206327
// Student Name : Desmond Koh, Herman Cho
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace COVID_Monitoring_System
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> personList = new List<Person>();
            List<BusinessLocation> businessLocationList = new List<BusinessLocation>();
            List<SHNFacility> SHNFacilityList = new List<SHNFacility>();

            while (true)
            {
                string option = DisplayMenu();

                if (option == "1")
                {
                    Console.WriteLine("\nLoad Person and Business Location Data");
                    LoadPerson(personList, SHNFacilityList);
                    LoadBusinessLocation(businessLocationList); ; ;
                }
                else if (option == "2")
                {
                    Console.WriteLine("\nLoad SHN Facility Data");
                    SHNFacilityList = LoadSHNFacilityData(SHNFacilityList);
                }
                else if (option == "3")
                {
                    Console.WriteLine("\nList all Visitors");
                    DisplayVisitor(personList);
                }
                else if (option == "4")
                {
                    Console.WriteLine("\nList Person Details");
                    ListPersonDetails(personList);
                }
                else if (option == "5")
                {
                    Console.WriteLine("\nAssign/Replace TraceTogether Token");
                    AssignAndReplaceToken(personList);
                }
                else if (option == "6")
                {
                    Console.WriteLine("\nList all Business Locations");
                    ListAllBusinessLocations(businessLocationList);
                }
                else if (option == "7")
                {
                    Console.WriteLine("\nEdit Business Location Capacity");
                    EditBusinessCapacity(businessLocationList);
                }
                else if (option == "8")
                {
                    Console.WriteLine("\nSafeEntry Check-in");
                    SafeEntryCheckin(businessLocationList, personList);
                }
                else if (option == "9")
                {
                    Console.WriteLine("\nSafeEntry Check-out");
                    SafeEntryCheckOut(businessLocationList, personList);
                }
                else if (option == "10")
                {
                    Console.WriteLine("\nList all SHN Facilities");
                    ListAllSHNFacilities(SHNFacilityList);
                }
                else if (option == "11")
                {
                    Console.WriteLine("\nCreate Visitor");
                    CreateVisitor(personList);
                }
                else if (option == "12")
                {
                    Console.WriteLine("\nCreate TravelEntry Record");
                    CreateTravelEntry(personList, SHNFacilityList);
                }
                else if (option == "13")
                {
                    Console.WriteLine("\nCalculate SHN Charges");
                    CalculateSHNCharges(personList);
                }
                else if (option == "14")
                {
                    Console.WriteLine("\nContact Tracing Reporting");
                    ContactTracingReporting(personList, businessLocationList);
                }
                else if (option == "15")
                {
                    Console.WriteLine("\nSHN Status Reporting");
                    SHNStatusReporting(personList);
                }
                else
                {
                    Console.WriteLine("\nInvalid Option. Please try again\n");
                }
            }
        }

        //Feature (1)
        static void LoadPerson(List<Person> personList, List<SHNFacility> shnFacilityList)
        {
            string[] lineArray = File.ReadAllLines("Person.csv");
            bool[] isPaid = new bool[100];
            DateTime[] shnEndDate = new DateTime[100];

            bool[] token = new bool[100];
            string[] tokenSerialNo = new string[100];
            string[] tokenCollection = new string[100];
            DateTime[] tokenExpire = new DateTime[100];

            int tokenCounter = 0;
            int travelEntryCounter = 0;
            for (int i = 1; i < lineArray.Length; i++)
            {
                string line = lineArray[i];
                string[] dataArray = line.Split(",");

                //Check if person is resident or visitor and load relevent information
                if (dataArray[0] == "resident")
                {
                    personList.Add(new Resident(dataArray[1], dataArray[2], Convert.ToDateTime(dataArray[3])));
                    if (dataArray[6] != "")
                    {
                        //Insert token information into array
                        token[tokenCounter] = true;
                        tokenSerialNo[tokenCounter] = dataArray[6];
                        tokenCollection[tokenCounter] = dataArray[7];
                        tokenExpire[tokenCounter] = Convert.ToDateTime(dataArray[8]);
                    }
                    else
                    {
                        token[tokenCounter] = false;
                    }
                    tokenCounter++;
                }
                else if (dataArray[0] == "visitor")
                {
                    personList.Add(new Visitor(dataArray[1], dataArray[4], dataArray[5]));
                }

                //Check if there is travel entry for the person and add it into array
                if (dataArray[9] != "")
                {
                    personList[i - 1].AddTravelEntry(new TravelEntry(dataArray[9], dataArray[10], Convert.ToDateTime(dataArray[11])));
                    isPaid[travelEntryCounter] = Convert.ToBoolean(dataArray[13]);
                    shnEndDate[travelEntryCounter] = Convert.ToDateTime(dataArray[12]);
                    travelEntryCounter++;
                }
            }

            //Load travel entry information from array into TravelEntry class
            for (int i = 0; i < personList.Count; i++)
            {
                List<TravelEntry> travelEntry = personList[i].TravelEntryList;
                foreach (TravelEntry t in travelEntry)
                {
                    t.isPaid = isPaid[i];
                    t.ShnEndDate = shnEndDate[i];
                }
            }

            //Load trace together token information from array into TraceTogetherToken class
            tokenCounter = 0;
            foreach (Person person in personList)
            {
                if (person is Resident)
                {
                    if (token[tokenCounter] == true)
                    {
                        Resident resident = (Resident)person;
                        resident.Token = new TraceTogetherToken(tokenSerialNo[tokenCounter], tokenCollection[tokenCounter], Convert.ToDateTime(tokenExpire[tokenCounter]));
                    }
                    else if (token[tokenCounter] == false)
                    {
                        Resident resident = (Resident)person;
                        resident.Token = new TraceTogetherToken(null, null, Convert.ToDateTime(null));
                    }
                    tokenCounter++;
                }
            }

            Console.WriteLine("=======================================================");
            Console.WriteLine("Person Data Loaded");
        }

        static void LoadBusinessLocation(List<BusinessLocation> businessLocationList)
        {
            //Load business location data
            string[] lineArray = File.ReadAllLines("BusinessLocation.csv");
            for (int i = 1; i < lineArray.Length; i++)
            {
                string line = lineArray[i];
                string[] dataArray = line.Split(",");
                businessLocationList.Add(new BusinessLocation(dataArray[0], dataArray[1], Convert.ToInt32(dataArray[2])));
            }
            Console.WriteLine("Business Location Data Loaded");
            Console.WriteLine("=======================================================\n");
        }

        //Feature(2)   
        static List<SHNFacility> LoadSHNFacilityData(List<SHNFacility> SHNFacilityList)
        {
            //Load SHN facility data from API
            Console.WriteLine("=======================================================");
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://covidmonitoringapiprg2.azurewebsites.net");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/facility");
                responseTask.Wait();

                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    string data = readTask.Result;
                    SHNFacilityList = JsonConvert.DeserializeObject<List<SHNFacility>>(data);
                    Console.WriteLine("SHN Facilities Loaded");
                    Console.WriteLine("=======================================================\n");
                    return SHNFacilityList;
                }
            }
            return null;
        }

        //Feature (3)
        static void DisplayVisitor(List<Person> personList)
        {
            Console.WriteLine("=======================================================");
            int i = 1;

            if (personList.Count != 0)
            {
                Console.WriteLine("{0,-4}{1,-9}{2,-15}{3,-9}", "", "Name", "Passport No", "Nationality");
                foreach (Person person in personList)
                {
                    //List every person that is a visitor
                    if (person is Visitor)
                    {
                        Visitor visitor = (Visitor)person;
                        Console.WriteLine("{0,-4}{1,-9}{2,-15}{3,-9}", i + ".", visitor.Name, visitor.PassportNo, visitor.Nationality);
                        i++;
                    }
                }
            }
            else
            {
                Console.WriteLine("\nThere are no visitors.");
            }

            Console.WriteLine("=======================================================\n");
        }

        //Feature(4)
        static void ListPersonDetails(List<Person> personList)
        {
            Console.WriteLine("=======================================================");
            Console.Write("Enter name: ");
            string name = Console.ReadLine();

            Person person = SearchPerson(personList, name);
            Console.Write("");

            //Check if person is visitor or resident and list relevent information
            if (person is Visitor)
            {
                Visitor visitor = (Visitor)person;
                Console.WriteLine("{0,-9}{1,-15}{2,-9}", "Name", "Passport No", "Nationality");
                Console.WriteLine("{0,-9}{1,-15}{2,-9}", visitor.Name, visitor.PassportNo, visitor.Nationality);
            }
            else if (person is Resident)
            {
                Resident resident = (Resident)person;
                Console.WriteLine("{0,-9}{1,-19}{2,-21}", "Name", "Address", "Last Left Country");
                Console.WriteLine("{0,-9}{1,-19}{2,-21}", resident.Name, resident.Address, resident.LastLeftCountry);
            }


            if (person != null)
            {
                //Check if there is any travel entry and list relevent information
                List<TravelEntry> travelEntryList = person.TravelEntryList;
                Console.WriteLine("\nTRAVEL ENTRY:");
                if (travelEntryList.Count != 0)
                {
                    Console.WriteLine("{0,-13}{1,-13}{2,-22}{3,-15}", "LCOE", "Entry Mode", "Entry Date", "SHN End Date");
                    foreach (TravelEntry t in travelEntryList)
                    {
                        t.CalculateSHNDuration();
                        Console.WriteLine("{0,-13}{1,-13}{2,-22}{3,-15}", t.LastCountryOfEmbarkation, t.EntryMode, t.EntryDateTime, t.ShnEndDate);
                    }
                }
                else
                {
                    Console.WriteLine("There are no travel entry information.");
                }

                //Check if there is any safe entry check in and list relevent information
                List<SafeEntry> safeEntryList = person.SafeEntryList;
                Console.WriteLine("\nSAFE ENTRY:");
                if (safeEntryList.Count != 0)
                {
                    Console.WriteLine("{0,-23}{1,-24}{2,-24}", "Business Name", "Check-In", "Check-Out");
                    foreach (SafeEntry s in safeEntryList)
                    {
                        Console.Write("{0,-23}{1,-24}", s.Location.BusinessName, s.CheckIn);
                        if (s.CheckOut == Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                        {
                            Console.WriteLine("Not checked out");
                        }
                        else
                        {
                            Console.WriteLine(s.CheckOut);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("There are no safe entry check in.");
                }

            }
            Console.WriteLine("=======================================================\n");
        }

        //Feature (5)
        static void AssignAndReplaceToken(List<Person> personList)
        {
            Console.WriteLine("============================================================");
            Console.Write("Enter resident name: ");
            string name = Console.ReadLine();

            Resident resident = SearchResident(personList, name);
            if (resident != null)
            {
                if (resident.Token.SerialNo != null)
                {
                    Console.WriteLine("{0,-12}{1,-15}{2,-15}", "Serial No", "Collection", "Expire");
                    Console.WriteLine("{0,-12}{1,-15}{2,-15}", resident.Token.SerialNo, resident.Token.CollectionLocation, resident.Token.ExpiryDate);

                    //Check for Token expiry
                    if (resident.Token.ExpiryDate <= DateTime.Now)
                    {
                        Console.WriteLine("\nThe Trace Together Token has been Expired.");

                        //Check if Token is eligible for replacement
                        if (resident.Token.IsEligibleForReplacement() == true)
                        {
                            Console.WriteLine("The Trace Together Token for " + resident.Name + " is eligible for replacement.\n");

                            Console.WriteLine("Make Replacement?:");
                            Console.WriteLine("1) Yes");
                            Console.WriteLine("2) No");
                            Console.Write("Enter option: ");
                            string replaceOption = Console.ReadLine();

                            if (replaceOption == "1")
                            {
                                resident.Token.ReplaceToken();
                            }
                            else if (replaceOption == "2")
                            {
                                Console.WriteLine("\nThe Trace Together Token has not been replace.");
                            }
                            else
                            {
                                Console.WriteLine("\nInvalid Option. Please try again.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("The Trace Together Token for " + resident.Name + " is not eligible for replacement.\n");
                        }
                    }
                }
                else
                {
                    //Assigning of Token
                    Console.WriteLine("There is no Trace Together Token for " + resident.Name);
                    Random num = new Random();

                    Console.Write("\nEnter token collection location: ");
                    string tokenCollection = Console.ReadLine();
                    resident.Token = new TraceTogetherToken("T" + num.Next(10000, 99999), tokenCollection, Convert.ToDateTime(DateTime.Now.AddMonths(6)));
                    Console.WriteLine("\nA Trace Together Token has been assigned to " + resident.Name);
                    Console.WriteLine("The Trace Together Token will exipre on " + DateTime.Now.AddMonths(6));
                }
            }
            Console.WriteLine("============================================================\n");
        }

        //Feature(6)
        static void ListAllBusinessLocations(List<BusinessLocation> businessLocationList)
        {
            Console.WriteLine("==========================================================");

            if (businessLocationList.Count != 0)
            {
                Console.WriteLine("{0,-4}{1,-24}{2,-14}{3,-20}", "", "Business Name", "Branch Code", "Maximum Capacity");
                int i = 1;
                foreach (BusinessLocation businesslocation in businessLocationList)
                {
                    Console.WriteLine("{0,-4}{1,-24}{2,-14}{3,-20}", i + ".", businesslocation.BusinessName, businesslocation.BranchCode, businesslocation.MaximumCapacity);
                    i++;
                }
            }
            else
            {
                Console.WriteLine("\nThere are no Business Location data. Please load it and try again.");
            }
            Console.WriteLine("==========================================================\n");
        }

        //Feature (7)
        static void EditBusinessCapacity(List<BusinessLocation> businessLocationList)
        {
            Console.WriteLine("======================================================================");
            int i = 1;
            if (businessLocationList.Count != 0)
            {
                //Listing of business locations
                foreach (BusinessLocation businessLocation in businessLocationList)
                {
                    Console.WriteLine(i + ") " + businessLocation.BusinessName);
                    i++;
                }
                Console.Write("\nSelect business: ");
                string locationToCheckIn = Console.ReadLine();

                string businessOption = "";
                if (locationToCheckIn == "1")
                {
                    businessOption = "ABC Spectacle Shop";
                }
                else if (locationToCheckIn == "2")
                {
                    businessOption = "Cheap Goods Shop";
                }
                else if (locationToCheckIn == "3")
                {
                    businessOption = "Big Shopping Centre";
                }
                else if (locationToCheckIn == "4")
                {
                    businessOption = "Bubble Tea Shop";
                }
                else
                {
                    Console.WriteLine("\nInvalid Option. Please try again.");
                }

                BusinessLocation business = SearchBusiness(businessLocationList, businessOption);
                if (business != null)
                {
                    Console.WriteLine("Current maximum capacity is " + business.MaximumCapacity + " for " + business.BusinessName + " (" + business.BranchCode + ")");
                    Console.Write("\nEnter new maximum capacity: ");

                    try
                    {
                        int newCapacity = Convert.ToInt32(Console.ReadLine());

                        //Check if entered capacity is not a negative value
                        if (newCapacity >= 0)
                        {
                            business.MaximumCapacity = newCapacity;
                            Console.WriteLine("\nThe new maximum capacity is set to " + business.MaximumCapacity);
                        }
                        else
                        {
                            Console.WriteLine("\nInvalid capacity. Please enter capacity of 0 and above.");
                        }
                    }
                    catch (System.FormatException)
                    {
                        Console.WriteLine("\nInvalid Option. Please try again.");
                    }
                }
            }
            else
            {
                Console.WriteLine("\nThere are no business location available. Please load it and try again.");
            }

            Console.WriteLine("======================================================================\n");
        }

        //Feature (8)
        static void SafeEntryCheckin(List<BusinessLocation> businessLocationList, List<Person> personList)
        {
            Console.WriteLine("===================================================================");
            Console.Write("Enter name: ");
            string name = Console.ReadLine();
            Person person = SearchPerson(personList, name);
            if (person != null)
            {
                if (businessLocationList.Count != 0)
                {
                    //Listing of business locations
                    Console.WriteLine("\nBusiness");
                    int i = 1;
                    foreach (BusinessLocation businessLocation in businessLocationList)
                    {
                        Console.WriteLine(i + ") " + businessLocation.BusinessName);
                        i++;
                    }
                    Console.Write("\nSelect Location to check-in: ");
                    string locationToCheckIn = Console.ReadLine();

                    string businessOption = "";
                    if (locationToCheckIn == "1")
                    {
                        businessOption = "ABC Spectacle Shop";
                    }
                    else if (locationToCheckIn == "2")
                    {
                        businessOption = "Cheap Goods Shop";
                    }
                    else if (locationToCheckIn == "3")
                    {
                        businessOption = "Big Shopping Centre";
                    }
                    else if (locationToCheckIn == "4")
                    {
                        businessOption = "Bubble Tea Shop";
                    }
                    else
                    {
                        Console.WriteLine("\nInvalid Option. Please try again.");
                    }

                    BusinessLocation business = SearchBusiness(businessLocationList, businessOption);
                    if (business != null)
                    {
                        //Check if business is full and add relevent information into SafeEntry class
                        if (business.isFull() == false)
                        {
                            business.VisitorsNow++;
                            person.AddSafeEntry(new SafeEntry(DateTime.Now, business));
                            Console.WriteLine("\nChecked in to " + businessOption + " at " + DateTime.Now);
                        }
                        else
                        {
                            Console.WriteLine("\nBusiness is full. Please wait to check in.");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("\nThere are no Business Location data. Please load it and try again.");
                }
            }
            Console.WriteLine("===================================================================\n");
        }

        //Feature (9)
        static void SafeEntryCheckOut(List<BusinessLocation> businessLocationList, List<Person> personList)
        {
            Console.WriteLine("===================================================================");
            Console.Write("Enter name: ");
            string name = Console.ReadLine();
            Person person = SearchPerson(personList, name);

            if (person != null)
            {
                List<SafeEntry> safeEntry = person.SafeEntryList;
                int i = 1;

                //Check if there is any safe entry check in for the given person
                if (person.SafeEntryList.Count != 0)
                {
                    //Ask user to select business location
                    Console.WriteLine("\nSelect Business:");
                    foreach (SafeEntry s in safeEntry)
                    {
                        Console.WriteLine(i + ") " + s.Location.BusinessName);
                        i++;
                    }

                    Console.Write("\nSelect location to check-out: ");
                    try
                    {
                        //Perform checkout
                        int locationToCheckOut = Convert.ToInt32(Console.ReadLine());

                        SafeEntry safeEntryBusiness = person.SafeEntryList[locationToCheckOut - 1];
                        safeEntryBusiness.PerformCheckOut();

                        BusinessLocation business = SearchBusiness(businessLocationList, safeEntryBusiness.Location.BusinessName);
                        business.VisitorsNow--;

                        Console.WriteLine("\nChecked out of " + safeEntryBusiness.Location.BusinessName + " at " + DateTime.Now);

                    }
                    catch (System.FormatException)
                    {
                        Console.WriteLine("\nInvalid Option. Please try again.");
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        Console.WriteLine("\nInvalid Option. Please try again.");
                    }

                }
                else
                {
                    Console.WriteLine("\nThere is no entry to check out");
                }
            }
            Console.WriteLine("===================================================================\n");
        }

        //Feature (10)
        static void ListAllSHNFacilities(List<SHNFacility> SHNFacilityList)
        {
            //List all SHN facilities
            int i = 1;
            Console.WriteLine("==================================================================================");

            if (SHNFacilityList.Count != 0)
            {
                Console.WriteLine("{0,-4}{1,-18}{2,-11}{3,-17}{4,-17}{5,-18}", "", "Name", "Capacity", "Air Checkpoint", "Sea Checkpoint", "Land Checkpoint");
                foreach (SHNFacility shnFacilityList in SHNFacilityList)
                {
                    Console.WriteLine("{0,-4}{1,-18}{2,-11}{3,-17}{4,-17}{5,-18}", i + ".",
                                       shnFacilityList.FacilityName, shnFacilityList.FacilityCapacity, shnFacilityList.DistFromAirCheckpoint + "km", shnFacilityList.DistFromSeaCheckpoint + "km", shnFacilityList.DistFromLandCheckpoint + "km");
                    i++;
                }
            }
            else
            {
                Console.WriteLine("\nThere are no SHN facility data. Please load it and try again.");
            }

            Console.WriteLine("==================================================================================\n");

        }

        //Feature (11)
        static void CreateVisitor(List<Person> personList)
        {
            //Ask visitor user for name, passport and nationality to create a new visitor
            Console.WriteLine("=======================================================");
            Console.Write("Enter Name: ");
            string name = Console.ReadLine();

            Console.Write("Enter Passport No: ");
            string passportNo = Console.ReadLine();

            Console.Write("Enter Nationality: ");
            string nationality = Console.ReadLine();

            personList.Add(new Visitor(name, passportNo, nationality));
            Console.WriteLine("\nVisitor added successfully");
            Console.WriteLine("=======================================================\n");
        }

        //Feature (12)
        static void CreateTravelEntry(List<Person> personList, List<SHNFacility> shnFacilityList)
        {
            Console.WriteLine("=======================================================");
            Console.Write("Enter Name: ");
            string name = Console.ReadLine();
            bool isPaid = false;
            string entryMode = "";

            Person person = SearchPerson(personList, name);
            if (person != null)
            {
                if (shnFacilityList.Count != 0)
                {
                    //Ask user for last country of embarkation
                    Console.Write("Enter Last Country of Embarkation: ");
                    string lastCountryOfEmbarkation = Console.ReadLine();

                    //Ask user for travel entry mode
                    while (true)
                    {
                        Console.WriteLine("\nTravel Entry Mode: ");
                        Console.WriteLine("1) Land");
                        Console.WriteLine("2) Sea");
                        Console.WriteLine("3) Air");
                        Console.Write("Enter option: ");
                        string entryModeOption = Console.ReadLine();

                        if (entryModeOption == "1")
                        {
                            entryMode = "Land";
                            break;
                        }
                        else if (entryModeOption == "2")
                        {
                            entryMode = "Sea";
                            break;
                        }
                        else if (entryModeOption == "3")
                        {
                            entryMode = "Air";
                            break;
                        }
                        else
                        {
                            Console.WriteLine("\nInvalid Option. Please try again.");
                        }
                    }

                    //Ask user for date of arrival
                    while (true)
                    {
                        Console.Write("\nEnter Date of Arrival (DD/MM/YYYY HH:MM:SS): ");
                        try
                        {
                            DateTime entryDateTime = Convert.ToDateTime(Console.ReadLine());
                            person.AddTravelEntry(new TravelEntry(lastCountryOfEmbarkation, entryMode, Convert.ToDateTime(entryDateTime)));
                            break;
                        }
                        catch (System.FormatException)
                        {
                            Console.WriteLine("\nDateTime is invalid. Please try again");
                        }
                    }

                    while (true)
                    {
                        try
                        {
                            if (lastCountryOfEmbarkation != "New Zealand" || lastCountryOfEmbarkation != "Vietnam")
                            {
                                //Ask user for that is serving SHN for SHN facility
                                Console.WriteLine("\nSHN is required to be served for " + person.Name);
                                int i = 1;

                                Console.WriteLine("SHN Facility:");
                                //List all SHN facilities
                                foreach (SHNFacility shnFacility in shnFacilityList)
                                {
                                    Console.WriteLine(i + ") " + shnFacility.FacilityName);
                                    i++;
                                }
                                Console.Write("Select a facility: ");
                                int facilityOption = Convert.ToInt32(Console.ReadLine());

                                SHNFacility facility = SearchFacility(shnFacilityList, shnFacilityList[facilityOption - 1].FacilityName);

                                List<TravelEntry> travelEntry = person.TravelEntryList;
                                foreach (TravelEntry t in travelEntry)
                                {
                                    t.AssignSHNFacility(facility);
                                }

                            }
                            break;
                        }
                        catch (System.FormatException)
                        {
                            Console.WriteLine("\nInvalid option. Please try again.");
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            Console.WriteLine("\nInvalid option. Please try again.");
                        }

                    }

                    //Ask user for travel charges payment
                    while (true)
                    {
                        try
                        {
                            Console.WriteLine("\nIs Travel Charges paid?:");
                            Console.WriteLine("1) Yes");
                            Console.WriteLine("2) No");
                            Console.Write("Enter option: ");
                            int isPaidOption = Convert.ToInt32(Console.ReadLine());

                            if (isPaidOption == 1)
                            {
                                isPaid = true;
                                break;
                            }
                            else if (isPaidOption == 2)
                            {
                                isPaid = false;
                                break;
                            }
                            else
                            {
                                Console.WriteLine("\nInvalid option. Please try again.");
                            }
                        }
                        catch (System.FormatException)
                        {
                            Console.WriteLine("\nInvalid option. Please try again.");
                        }
                    }

                    //Add travel entry details to TravelEntry class
                    List<TravelEntry> travelEntryList = person.TravelEntryList;
                    foreach (TravelEntry travelEntry in travelEntryList)
                    {
                        travelEntry.isPaid = Convert.ToBoolean(isPaid);
                        travelEntry.CalculateSHNDuration();
                    }
                    Console.WriteLine("\nA new travel entry has been created for " + person.Name);
                }
                else
                {
                    Console.WriteLine("\nThere are no SHN facility data. Please load it and try again.");
                }
            }
            Console.WriteLine("=======================================================\n");
        }

        //Feature (13)
        static void CalculateSHNCharges(List<Person> personList)
        {
            Console.WriteLine("=======================================================");
            Console.Write("Enter name: ");
            string name = Console.ReadLine();

            Person person = SearchPerson(personList, name);
            if (person != null)
            {
                List<TravelEntry> travelEntryList = person.TravelEntryList;
                foreach (TravelEntry t in travelEntryList)
                {
                    double charge = 0;
                    if ((t.LastCountryOfEmbarkation != "New Zealand" || t.LastCountryOfEmbarkation != "Vietnam" || t.LastCountryOfEmbarkation != "Macao SAR") && person is Visitor)
                    {
                        charge = (person.CalculateSHNCharges() + t.ShnStay.CalculateTravelCost(t.EntryMode, t.EntryDateTime)) * 1.07;
                    }
                    else
                    {
                        charge = person.CalculateSHNCharges() * 1.07;
                    }

                    //Check if payment is not paid and request payment
                    t.CalculateSHNDuration();
                    if (t.isPaid == false)
                    {
                        Console.WriteLine("{0,-9}{1,-15}{2,-10}{3,-10}", "Name", "SHN Charge", "Status", "SHN End Date");
                        Console.WriteLine("{0,-9}{1,-15}{2,-10}{3,-10}", person.Name, "$" + charge, "Unpaid", t.ShnEndDate);
                        Console.WriteLine("\nMake payment");
                        Console.WriteLine("1) Yes");
                        Console.WriteLine("2) No");
                        Console.Write("Enter option: ");
                        int paymentOption = Convert.ToInt32(Console.ReadLine());

                        if (paymentOption == 1)
                        {
                            Console.WriteLine("\nPayment of $" + charge + " has been made successfully");
                            t.isPaid = true;
                        }
                        else
                        {
                            Console.WriteLine("\nPayment cancelled");
                        }
                    }
                    else
                    {
                        t.CalculateSHNDuration();
                        Console.WriteLine("{0,-9}{1,-15}{2,-10}{3,-10}", "Name", "SHN Charge", "Status", "SHN End Date");
                        Console.WriteLine("{0,-9}{1,-15}{2,-10}{3,-10}", person.Name, "$" + charge, "Paid", t.ShnEndDate);
                    }
                }
            }
            Console.WriteLine("=======================================================\n");
        }

        //Advance Feature (1)
        static void ContactTracingReporting(List<Person> personList, List<BusinessLocation> businessLocationList)
        {
            Console.WriteLine("=======================================================");
            List<string> lines = new List<string>();
            lines.Add("Name, Check-In, Check-Out");
            string fileName = "";
            string businessName = "";

            if (businessLocationList.Count != 0)
            {
                //Listing of business locations
                Console.WriteLine("Business");
                int i = 1;
                foreach (BusinessLocation businessLocation in businessLocationList)
                {
                    Console.WriteLine(i + ") " + businessLocation.BusinessName);
                    i++;
                }
                Console.Write("\nSelect Location to trace: ");
                string locationToCheckIn = Console.ReadLine();

                if (locationToCheckIn == "1")
                {
                    businessName = "ABC Spectacle Shop";
                }
                else if (locationToCheckIn == "2")
                {
                    businessName = "Cheap Goods Shop";
                }
                else if (locationToCheckIn == "3")
                {
                    businessName = "Big Shopping Centre";
                }
                else if (locationToCheckIn == "4")
                {
                    businessName = "Bubble Tea Shop";
                }
                else
                {
                    Console.WriteLine("\nInvalid Option. Please try again.");
                }

                if (SearchBusiness(businessLocationList, businessName) != null)
                {
                    try
                    {
                        //Ask user for date and time
                        Console.Write("Enter Date and Time (DD/MM/YYYY HH:MM:SS in 24hr): ");
                        DateTime dateTime = Convert.ToDateTime(Console.ReadLine());

                        foreach (Person p in personList)
                        {
                            foreach (SafeEntry s in p.SafeEntryList)
                            {
                                if (s.CheckOut == Convert.ToDateTime("1/1/0001 12:00:00 AM"))
                                {
                                    continue;
                                }
                                //Check for any check in during period given
                                else if (s.CheckIn <= dateTime && dateTime <= s.CheckOut)
                                {
                                    if (s.Location.BusinessName == businessName)
                                    {
                                        lines.Add(p.Name + "," + s.CheckIn + "," + s.CheckOut);
                                        fileName = businessName.Replace(" ", string.Empty) + Convert.ToString(dateTime).Replace(" ", string.Empty).Replace("/", string.Empty).Replace(":", string.Empty).Replace("AM", string.Empty).Replace("PM", string.Empty);
                                    }
                                }
                            }
                        }

                        if (lines.Count == 1)
                        {
                            Console.WriteLine("\nThere is no one checked-in to " + businessName + " during that period.");
                        }
                        else
                        {
                            try
                            {
                                //Generate file
                                File.WriteAllLines("ContactTracingReport\\" + fileName + ".csv", lines);
                            }
                            catch (DirectoryNotFoundException)
                            {
                                Console.WriteLine("\nThe folder 'ContactTracingReport' does not exist.");
                                Console.WriteLine("The report will not be created in the ContactTracingReport folder.");
                                File.WriteAllLines(fileName + ".csv", lines);
                            }
                            Console.WriteLine("\nContact tracing report has been created.");
                        }
                    }
                    catch (System.FormatException)
                    {
                        Console.WriteLine("\nDateTime is invalid. Please try again");
                        Console.WriteLine("Please check if the time is in 24hr format");
                    }
                }
            }
            else
            {
                Console.WriteLine("\nThere are no Business Location data. Please load it and try again.");
            }

            Console.WriteLine("=======================================================\n");
        }

        //Advanced Feature (2)
        static void SHNStatusReporting(List<Person> personList)
        {
            Console.WriteLine("=======================================================");
            List<string> lines = new List<string>();
            lines.Add("Name, SHN End Date");
            string fileName = "";

            try
            {
                Console.Write("Enter Date and Time (DD/MM/YYYY HH:MM:SS in 24hr): ");
                DateTime dateTime = Convert.ToDateTime(Console.ReadLine());

                foreach (Person p in personList)
                {
                    foreach (TravelEntry TE in p.TravelEntryList)
                    {
                        TE.CalculateSHNDuration();
                        //Check for any stays in SHN Facilities during period given
                        if (TE.EntryDateTime <= dateTime && dateTime <= TE.ShnEndDate)
                        {
                            lines.Add(p.Name + "," + TE.ShnEndDate);
                            fileName = "SHNStatusReport" + Convert.ToString(dateTime).Replace(" ", string.Empty).Replace("/", string.Empty).Replace(":", string.Empty).Replace("AM", string.Empty).Replace("PM", string.Empty);
                        }
                    }
                }

                if (lines.Count == 1)
                {
                    Console.WriteLine("\nThere is no one serving SHN during that period");
                }
                else
                {
                    try
                    {
                        //Generate file
                        File.WriteAllLines("SHNStatusReport\\" + fileName + ".csv", lines);
                    }
                    catch (DirectoryNotFoundException)
                    {
                        Console.WriteLine("\nThe folder 'SHNStatusReport' does not exist.");
                        Console.WriteLine("The report will not be created in the SHNStatusReport folder.");
                        File.WriteAllLines(fileName + ".csv", lines);
                    }
                    Console.WriteLine("\nSHN Status report has been created.");

                }
            }
            catch (System.FormatException)
            {
                Console.WriteLine("\nDateTime is invalid. Please try again");
                Console.WriteLine("Please check if the time is in 24hr format");
            }

            Console.WriteLine("=======================================================\n");
        }


        //Display Menu
        static string DisplayMenu()
        {
            Console.WriteLine("=== General ===");
            Console.WriteLine("1) Load Person and Business Location Data");
            Console.WriteLine("2) Load SHN Facility Data");
            Console.WriteLine("3) List all Visitors");
            Console.WriteLine("4) List Person Details");
            Console.WriteLine("\n=== SafeEntry/TraceTogether ===");
            Console.WriteLine("5) Assign/Replace TraceTogether Token");
            Console.WriteLine("6) List all Business Locations");
            Console.WriteLine("7) Edit Business Location Capacity");
            Console.WriteLine("8) SafeEntry Check-in");
            Console.WriteLine("9) SafeEntry Check-out");
            Console.WriteLine("\n=== TravelEntry ===");
            Console.WriteLine("10) List all SHN Facilities");
            Console.WriteLine("11) Create Visitor");
            Console.WriteLine("12) Create TravelEntry Record");
            Console.WriteLine("13) Calculate SHN Charges");
            Console.WriteLine("\n=== Reporting ===");
            Console.WriteLine("14) Contact Tracing Reporting");
            Console.WriteLine("15) SHN Status Reporting");
            Console.Write("\nEnter your option: ");
            string option = Console.ReadLine();
            return option;
        }

        // Search Function
        static Person SearchPerson(List<Person> personList, string name)
        {
            foreach (Person person in personList)
            {
                if (name.ToLower() == person.Name.ToLower())
                {
                    return person;
                }
            }
            Console.WriteLine("\nPerson not found");
            return null;
        }

        static Resident SearchResident(List<Person> personList, string name)
        {
            foreach (Person person in personList)
            {
                if (person is Resident)
                {
                    Resident resident = (Resident)person;
                    if (name.ToLower() == resident.Name.ToLower())
                    {
                        return resident;
                    }
                }
            }
            Console.WriteLine("\nResident not found");
            return null;
        }

        static BusinessLocation SearchBusiness(List<BusinessLocation> businessLocationList, string businessName)
        {
            foreach (BusinessLocation business in businessLocationList)
            {
                if (businessName.ToLower() == business.BusinessName.ToLower())
                {
                    return business;
                }
            }
            Console.WriteLine("\nBusiness not found");
            return null;
        }

        static SHNFacility SearchFacility(List<SHNFacility> shnFacilityList, string facilityName)
        {
            foreach (SHNFacility facility in shnFacilityList)
            {
                if (facilityName.ToLower() == facility.FacilityName.ToLower())
                {
                    return facility;
                }
            }
            Console.WriteLine("\nFacility not found");
            return null;
        }
    }
}
