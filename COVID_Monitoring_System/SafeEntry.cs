//============================================================
// Student Number : S10208193, S10206327
// Student Name : Desmond Koh, Herman Cho
// Module Group : T03
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace COVID_Monitoring_System
{
    class SafeEntry
    {
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public BusinessLocation Location { get; set; }

        public SafeEntry() { }

        public SafeEntry(DateTime checkin, BusinessLocation businessLocation)
        {
            CheckIn = checkin;
            Location = businessLocation;
        }

        public void PerformCheckOut()
        {
            CheckOut = DateTime.Now;
        }

        public override string ToString()
        {
            return "CheckOutTime: " + CheckOut +
                "\nLocation: " + Location;
        }
    }
}
